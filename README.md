# Bitpanda QA 4

SQL part

## Question 

Please write an SQL query:

● Table “users” with columns (id, email, citizenship_country_id)

● Table “countries” with columns (id, name, iso)

● Show all countries with more than 1000 users, sorted by user count. The
country with the most users should be at the top.

## Answer

Assuming we create DB like this:

```
 CREATE TABLE countries (
  id int NOT NULL,
  name varchar(255),
  iso varchar(255),
  PRIMARY KEY (id)
 );
  
 CREATE TABLE users (
  id int NOT NULL,
  email varchar(255),
  citizenship_country_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (citizenship_country_id) REFERENCES countries(id)
 );
```

Query will look like this:

```
SELECT country.iso AS country, Count(*) AS userCount
FROM countries country JOIN
     users user
     ON country.id = user.citizenship_country_id
GROUP BY country.name
HAVING COUNT(*) > 1000
ORDER BY userCount DESC;
```

Checked on fiddle, mysql 5.6

http://sqlfiddle.com/#!9/084549/10

## PS
You may want to change this assignment lol

https://stackoverflow.com/questions/67202158/join-with-count
